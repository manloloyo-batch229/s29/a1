
// 1.
db.courses.find({
    
    $and: [
    
        {instructor:{$regex:'sir',$options: '$i'}},
        {price:{$gte:20000}}
    
    
    
    ]
    
    
    
    })

// 2.
db.courses.find({instructor:"Ma'am Tine"},{_id:0,name:1})

// 3.
db.courses.find({instructor:"Ma'am Miah"},{_id:0,name:1,price:1})


// 4.
db.courses.find({
       $and: [

            {name:{$regex:'r',$options:'$i'}},
            {price:{$gte:20000}}
    
    ]

    })

// 5.
db.courses.find({
     $and: [
        {isActive:true},
        {price:{$lte:25000}}
    
    
    
    ]
    
    })



